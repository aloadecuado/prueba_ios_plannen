//
//  CostomerModel.swift
//  PruebaIosPlannen
//
//  Created by Propio Data on 6/5/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation

class CustomerModel: NSObject {
    var customerId = ""
    var nombre = ""
    
    var apellido = ""
    var direccion = ""
    
    var telefono = ""
    var estado = ""
    
    var userId = ""
    
    init(dic:NSDictionary){
        customerId = ValueJsonString(dic: dic, key: "customerId")
        nombre = ValueJsonString(dic: dic, key: "nombre")
        
        apellido = ValueJsonString(dic: dic, key: "apellido")
        direccion = ValueJsonString(dic: dic, key: "direccion")
        
        telefono = ValueJsonString(dic: dic, key: "telefono")
        estado = ValueJsonString(dic: dic, key: "estado")
        
        userId = ValueJsonString(dic: dic, key: "userId")
    }
    
    func getDic() -> NSDictionary{
        return ["customerId":customerId,"nombre":nombre,"apellido":apellido,"direccion":direccion,"telefono":telefono,"estado":estado,userId:"userId"]
    }
    
    func getJsonString() -> String{
        
        do{
            let jsonData: NSData = try JSONSerialization.data(withJSONObject: getDic(), options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
            
            return NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
        }catch{
            return ""
        }
        
        
    }
}
