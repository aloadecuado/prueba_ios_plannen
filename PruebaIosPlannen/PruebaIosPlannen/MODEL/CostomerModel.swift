//
//  CostomerModel.swift
//  PruebaIosPlannen
//
//  Created by Propio Data on 6/5/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation

class CustomerModel: NSObject {
    var succes = false
    var resultMessage = ""
    var userId = ""
    
    init(dic:NSDictionary){
        succes = ValueJsonBool(dic: dic, key: "succes")
        resultMessage = ValueJsonString(dic: dic, key: "resultMessage")
        userId = ValueJsonString(dic: dic, key: "userId")
    }
    
    func getDic() -> NSDictionary{
        return ["succes":succes,"resultMessage":resultMessage,"userId":userId]
    }
}
