//
//  CustomersViewController.swift
//  PruebaIosPlannen
//
//  Created by Propio Data on 6/5/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import UIKit
import Material
class CustomersViewController: UIViewController {

    @IBOutlet weak var tableView:UITableView!
    var user = UserModel(dic: ["":""])
    var customers = [CustomerModel]()
    var Custom = CustomerModel(dic: ["":""])
    let textCellIdentifier = "CellID"
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        getListCustomer(controller: self, userId: USER.userId, Ok: {array in
            self.customers = array
            self.tableView.reloadData()
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail"{
            let view =  segue.destination as! DetailCustomerViewController
            view.customer = Custom
        }
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}

extension CustomersViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath) as! CustomersViewCell
        
        cell.etFullName.text = customers[indexPath.row].nombre + " " + customers[indexPath.row].apellido
        cell.etNumberId.text = customers[indexPath.row].customerId
        cell.etNumberPhone.text = customers[indexPath.row].telefono
        print("Estado: \(customers[indexPath.row].estado)")
        switch (customers[indexPath.row].estado){
        case "0":
            cell.vGeneral.backgroundColor = UIColor.yellow

            break;
        case "1":
            cell.vGeneral.backgroundColor = UIColor.blue
            
            break;
        case "2":
            cell.vGeneral.backgroundColor = UIColor.green
            
            break;
        case "3":
            cell.vGeneral.backgroundColor = UIColor.red
            
            break;
        case "4":
            cell.vGeneral.backgroundColor = UIColor.gray
            
            break;
        default:
            cell.vGeneral.backgroundColor = UIColor.yellow
            break;
        }

        
        return cell;
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
Custom = customers[indexPath.row]
        self.performSegue(withIdentifier: "showDetail", sender: nil)
        
    }
    
    
}

class CustomersViewCell: UITableViewCell{
    @IBOutlet weak var etFullName:IndetTextField!
    @IBOutlet weak var etNumberId:IndetTextField!
    @IBOutlet weak var etNumberPhone:IndetTextField!
    @IBOutlet weak var vGeneral:UIView!
}
