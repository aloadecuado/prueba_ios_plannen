//
//  DetailCustomerViewController.swift
//  PruebaIosPlannen
//
//  Created by Propio Data on 6/6/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import UIKit
import Material
class DetailCustomerViewController: UIViewController {

    @IBOutlet weak var sgState:UISegmentedControl!
    @IBOutlet weak var etId:IndetTextField!
    @IBOutlet weak var etName:IndetTextField!
    @IBOutlet weak var etLastName:IndetTextField!
    @IBOutlet weak var etPhone:IndetTextField!
    @IBOutlet weak var etAddress:IndetTextField!
    @IBOutlet weak var svGeneral: UIScrollView!
    
    var customer = CustomerModel(dic: ["":""])
    var estado = "0";
    override func viewDidLoad() {
        super.viewDidLoad()

        etId.text = customer.customerId
        etName.text = customer.nombre
        etLastName.text = customer.apellido
        etPhone.text = customer.telefono
        etAddress.text = customer.direccion
        
        svGeneral.contentSize.height =  600.0
        estado = customer.estado;
        switch (customer.estado){
        case "0":
            view.backgroundColor = UIColor.yellow
            
            break;
        case "1":
            view.backgroundColor = UIColor.blue
            
            break;
        case "2":
            view.backgroundColor = UIColor.green
            
            break;
        case "3":
            view.backgroundColor = UIColor.red
            
            break;
        case "4":
            view.backgroundColor = UIColor.gray
            
            break;
        default:
            view.backgroundColor = UIColor.yellow
            break;
        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveCustomer(button:Button){
        customer.nombre = etName.text!
        customer.apellido = etLastName.text!
        customer.telefono = etPhone.text!
        customer.direccion = etAddress.text!
        customer.estado = estado
        saveData(customer: customer)
        
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func indexChanged(_ sender: Any) {
        switch sgState.selectedSegmentIndex
        {
        case 0:
            estado = "0"
            view.backgroundColor = UIColor.yellow
            break;
        case 1:
            estado = "1"
            view.backgroundColor = UIColor.blue
            break;
        case 2 :
            estado = "2"
            view.backgroundColor = UIColor.green
            break;
        case 3:
            estado = "3"
            view.backgroundColor = UIColor.red
            break;
        case 4:
            estado = "4"
            view.backgroundColor = UIColor.gray
            break;
        default:
            break
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
