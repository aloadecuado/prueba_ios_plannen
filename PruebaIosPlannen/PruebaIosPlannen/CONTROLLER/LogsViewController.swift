//
//  LogsViewController.swift
//  PruebaIosPlannen
//
//  Created by Propio Data on 6/6/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import UIKit

class LogsTableViewController: UITableViewController {

    var logs = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        logs = GetDataLogsr()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! LogsTableViewControllerCell
        
        cell.lbLog.text = logs[indexPath.row]
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
            return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return logs.count
    }

}

class LogsTableViewControllerCell:UITableViewCell{
    @IBOutlet weak var lbLog:UILabel!
}



