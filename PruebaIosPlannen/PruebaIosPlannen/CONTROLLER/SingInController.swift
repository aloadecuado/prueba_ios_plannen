//
//  ViewController.swift
//  PruebaIosPlannen
//
//  Created by Propio Data on 6/5/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import UIKit
import Material
class SingInController: UIViewController {
    @IBOutlet weak var etEmail: IndetTextField!
    @IBOutlet weak var etPass: IndetTextField!
    
    var user = UserModel(dic: ["":""])
    override func viewDidLoad() {
        super.viewDidLoad()
        
        etEmail.setTextoEmail()
        etPass.setTexto(Rango: 20)
        

        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.user = getAutoSignIn()
        if user.succes{
            USER = self.user
            self.performSegue(withIdentifier: "showList", sender: nil)
        }
    }

    @IBAction func signInPressed(_ sender: Button) {
        signIn(controller: self, view: self.view, etEmail: etEmail, etPass: etPass, Ok: {user in
            self.user = user
            USER = self.user
            self.performSegue(withIdentifier: "showList", sender: nil)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        

        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}

