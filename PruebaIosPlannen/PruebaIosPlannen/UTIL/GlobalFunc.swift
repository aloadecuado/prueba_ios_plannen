//
//  GlobalFunc.swift
//  co.facilpay.facilpayiostemporal
//
//  Created by Pedro Alonso Daza B on 29/09/18.
//  Copyright © 2018 Pedro Alonso Daza B. All rights reserved.
//

import Foundation

import UIKit
import Material
import AVFoundation
import Foundation

func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

func getJsonArrayString(array:[CustomerModel]) -> String{
    
    var dics = [NSDictionary]()
    
    for custom in array{
        dics.append(custom.getDic())
    }
    do{
        let jsonData: NSData = try JSONSerialization.data(withJSONObject: dics, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
        
        return NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
    }catch{
        return ""
    }
    
    
}

func convertToArray(text: String) -> NSArray? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? NSArray
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}
func ValidateStringContent(string:String, Exist:String) -> Bool
{
    
    if string == ""
    {
        return true
    }
    return Exist.contains(string)
}

func ValidationKeyNumbers(s:String, range:Int) -> Bool
{
    let aSet = NSCharacterSet(charactersIn: SNumbersKey).inverted
    //let aSet = NSCharacterSet(charactersInString:SNumbersKey).invertedSet
    let compSepByCharInSet = s.components(separatedBy: aSet)
    //let compSepByCharInSet = s.componentsSeparatedByCharactersInSet(aSet)
    let numberFiltered = compSepByCharInSet.joined(separator: "")
    
    
    return s == numberFiltered && s.count <= range
}


func ValidationEmail(testStr:String) -> Bool {
    // print("validate calendar: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let isemail = emailTest.evaluate(with: testStr)
    return isemail
}

func ChangeCurrencyAmount(tipAmount: Int) -> String
{
    let formatter = NumberFormatter()
    formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
    formatter.numberStyle = .currency
    if let formattedTipAmount = formatter.string(from: tipAmount as NSNumber) {
        return ": \(formattedTipAmount)"
    }
    else
    {
        return "$ 0"
    }
}
func CambioFormatoLetras (SCifra: String, MaxLong:String, MinLong:String) -> String
{

    print("\(Int64.max)")
    var Cifra:Int64 = 0
    let sNumerop = SCifra
    if sNumerop != ""
    {
        let num = sNumerop.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: "'", with: "").replacingOccurrences(of: "$", with: "")
        //let num = sNumerop.stringByReplacingOccurrencesOfString(".", withString: "").stringByReplacingOccurrencesOfString("'", withString: "").stringByReplacingOccurrencesOfString("$", withString: "")
        if let Lnum = Int64(num)
        {
            Cifra = Lnum
            
            if Cifra > Int64(MaxLong)!
            {
                Cifra =  Int64(MaxLong)!
            }
            else if Cifra < Int64(MinLong)!
            {
                Cifra = Int64(MinLong)!
            }
        }
        else
        {
            return "0"
        }
    }
    else
    {
        return "0"
    }
    if Cifra <= 999
    {
        return "$\(Cifra)"
    }
    else if Cifra >= 1000 && Cifra <= 999999
    {
        let mil = Cifra / 1000
        let cen = Cifra - (mil * 1000)
        var scen = ""
        if "\(cen)".count <= 2
        {
            
            if "\(cen)".count == 1
            {
                scen = "00\(cen)"
            }
            else if "\(cen)".count == 2
            {
                scen = "0\(cen)"
            }
        }
        else
        {
            scen = "\(cen)"
        }
        return "$\(mil).\(scen)"
    }
    else if Cifra >= 1000000 && Cifra <= 999999999
    {
        let mill = Cifra / 1000000
        let mil = (Cifra - (mill * 1000000)) / 1000
        var smil = ""
        
        var scen = ""
        if "\(mil)".count <= 2
        {
            
            if "\(mil)".count == 1
            {
                smil = "00\(mil)"
            }
            else if "\(mil)".count == 2
            {
                smil = "0\(mil)"
            }
        }
        else
        {
            smil = "\(mil)"
        }
        let cen = Cifra - Int64("\(mill)\(smil)000")!
        if "\(cen)".count <= 2
        {
            
            if "\(cen)".count == 1
            {
                scen = "00\(cen)"
            }
            else if "\(cen)".count == 2
            {
                scen = "0\(cen)"
            }
        }
        else
        {
            scen = "\(cen)"
        }
        
        return "$\(mill).\(smil).\(scen)"
    }
    else
    {
        let milll = Cifra / 1000000000
        let mill = (Cifra - (milll * 1000000000)) / 1000000
        
        
        
        
        var smill = ""
        var smil = ""
        var scen = ""
        if "\(mill)".count <= 2
        {
            
            if "\(mill)".count == 1
            {
                smill = "00\(mill)"
            }
            else if "\(mill)".count == 2
            {
                smill = "0\(mill)"
            }
        }
        else
        {
            smill = "\(mill)"
        }
        let mil = (Cifra - Int64("\(milll)\(smill)000000")!) / 1000
        if "\(mil)".count <= 2
        {
            print("Contador chars milles: \("\(mil)".count)")
            
            if "\(mil)".count == 1
            {
                smil = "00\(mil)"
            }
            else if "\(mil)".count == 2
            {
                smil = "0\(mil)"
            }
            else
            {
                smil = "707"
            }
        }
        else
        {
            smil = "\(mil)"
        }
        
        let cen = Cifra - Int64("\(milll)\(smill)\(smil)000")!
        if "\(cen)".count <= 2
        {
            
            if "\(cen)".count == 1
            {
                scen = "00\(cen)"
            }
            else if "\(cen)".count == 2
            {
                scen = "0\(cen)"
            }
        }
        else
        {
            scen = "\(cen)"
        }
        
        
        return "$\(milll).\(smill).\(smil).\(scen)"
    }
    
    
}




func ValidationKeyDecimal(s:String, range:Int) -> Bool
{
    
    
    if s.components(separatedBy: ".").count >= 3
    {
        return false
    }
    let aSet = NSCharacterSet(charactersIn: SDecimalKey).inverted
    let compSepByCharInSet = s.components(separatedBy: aSet)
    let numberFiltered = compSepByCharInSet.joined(separator: "")
    
    
    return s == numberFiltered && s.count <= range
}
func ValidationKeyNumbersMax(s:String, range:Int, max: Int) -> Bool
{
    let aSet = NSCharacterSet(charactersIn: SNumbersKey).inverted
    let compSepByCharInSet = s.components(separatedBy: aSet)
    let numberFiltered = compSepByCharInSet.joined(separator: "")
    
    
    return s == numberFiltered && s.count <= range && Int(s)! <= max
}

func ValidationEmptyExeption(viewc: UIView, Ets:[TextField]) -> Bool
{
    var permiso = true
    for vv in viewc.subviews as [UIView]
    {
        if vv is TextField
        {
            let textff = vv as! TextField
            if let ppholder = textff.placeholder
            {
                //KdevPrint(Men: "placeholder: \(ppholder)")
                //KdevPrint(Men: "text: \(textff.text!)")
                var esException = false
                for tt in Ets
                {
                    if textff == tt
                    {
                        esException = true
                        break
                    }
                }
                
                if textff.text == "" && !esException
                {
                    if textff.isHidden == false
                    {
                        textff.detail = ErrorEmpty
                        permiso = false
                    }
                }
                
            }
            
            
        }
    }
    
    return permiso
}

class Colors {
    var gl:CAGradientLayer!
    
    init() {
        let colorTop = UIColor(red: 192.0 / 255.0, green: 38.0 / 255.0, blue: 42.0 / 255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 35.0 / 255.0, green: 2.0 / 255.0, blue: 2.0 / 255.0, alpha: 1.0).cgColor
        
        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [0.0, 1.0]
    }
}

func encryptMessage(message: String, encryptionKey: String) -> String {
    let messageData = message.data(using: .utf8)!
    //let cipherData = RNCryptor.encrypt(data: messageData, withPassword: encryptionKey)
    let base64 = messageData.base64EncodedString()
    return base64
}

func decryptMessage(encryptedMessage: String, encryptionKey: String) -> String {
    
    let encryptedData = Data.init(base64Encoded: encryptedMessage)!
    //let decryptedData = try RNCryptor.decrypt(data: encryptedData, withPassword: encryptionKey)
    let decryptedString = String(data: encryptedData, encoding: .utf8)!
    let base64 = decryptedString
    return base64
}



func getIFAddresses() -> [String] {
    var addresses = [String]()
    
    // Get list of all interfaces on the local machine:
    var ifaddr : UnsafeMutablePointer<ifaddrs>?
    guard getifaddrs(&ifaddr) == 0 else { return [] }
    guard let firstAddr = ifaddr else { return [] }
    
    // For each interface ...
    for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
        let flags = Int32(ptr.pointee.ifa_flags)
        let addr = ptr.pointee.ifa_addr.pointee
        
        // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
        if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
            if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                
                // Convert interface address to a human readable string:
                var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                    let address = String(cString: hostname)
                    addresses.append(address)
                }
            }
        }
    }
    
    freeifaddrs(ifaddr)
    return addresses
}

func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}


