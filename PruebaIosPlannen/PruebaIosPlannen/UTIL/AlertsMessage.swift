//
//  AlertsMessage.swift
//  PruebaIosIntergroup
//
//  Created by Pedro Alonso Daza B on 27/07/18.
//  Copyright © 2018 Pedro Alonso Daza B. All rights reserved.
//

import Foundation
import UIKit

func showAlertError(View: UIViewController, Men: String)
{
    let alertController = UIAlertController(title: "Información", message:
        Men, preferredStyle: UIAlertController.Style.alert)
    alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default,handler: nil))
    
    View.present(alertController, animated: true, completion: nil)
}

func showAlertErrorComplete(View: UIViewController, Men: String, Complation:@escaping (() -> Void) )
{
    let alertController = UIAlertController(title: "Información", message:
        Men, preferredStyle: UIAlertController.Style.alert)
    alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default,handler: { (action: UIAlertAction!) in
        Complation()
        
    }))
    
    View.present(alertController, animated: true, completion: {
        Complation()
    })
}
func showAlertSiONo(View: UIViewController, Men: String, Si:@escaping (() -> Void), No:@escaping (() -> Void) )
{
    
    let refreshAlert = UIAlertController(title: "Advertencia!", message: Men, preferredStyle: UIAlertController.Style.alert)
    
    refreshAlert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
        No()
        
    }))
    
    refreshAlert.addAction(UIAlertAction(title: "Si", style: .cancel, handler: { (action: UIAlertAction!) in
        Si()
        
        
    }))
    
    /*let alertController = UIAlertController(title: "Informacion", message:
     Men, preferredStyle: UIAlertControllerStyle.Alert)
     alertController.addAction(UIAlertAction(title: "Cerrar", style: UIAlertActionStyle.Default,handler: nil))*/
    
    View.present(refreshAlert, animated: true, completion: nil)
}
