//
//  GlobalVar.swift
//  co.facilpay.facilpayiostemporal
//
//  Created by Pedro Alonso Daza B on 29/09/18.
//  Copyright © 2018 Pedro Alonso Daza B. All rights reserved.
//

import Foundation
import UIKit

let SECRETKEY = "F4C1lP4y.831024$";
let leng = "Esp"
let PRODUCCION = false
let KURL = (PRODUCCION) ? "https://plannen.com/" : "https://plannen.com/"
let KURLUser = KURL + "appServices/test/login.php?email={email}&pass={pass}"
let KURLListCustomer = KURL + "appServices/test/customers.php?userId={userId}"


let VALIDATIONALFABETO = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLNMÑOPQUVWRSTXYZ "
let VALIDATIONALFANUMERICO = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLNMÑOPQRSTWUVXYZ1234567890 "

let ErrosEmail = (leng == "Esp") ? "No es un email valido." : ""
let ErrorEmpty = (leng == "Esp") ? "Por favor Diligenciar este campo." : ""


let SNumbersKey = "0123456789"
let SDecimalKey = "0123456789."

//LOCALVAR GLOBAL
var USER = UserModel(dic: ["":""])


