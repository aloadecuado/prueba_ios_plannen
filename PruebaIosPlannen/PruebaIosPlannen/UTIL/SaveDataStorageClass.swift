//
//  SaveDataStorageClass.swift
//  co.facilpay.facilpayiostemporal
//
//  Created by Pedro Alonso Daza B on 11/3/18.
//  Copyright © 2018 Pedro Alonso Daza B. All rights reserved.
//

import Foundation


func setDataSignIn(UserString: String)//, product:String)
{
    let Id = "IdDataUser"
    
    
    UserDefaults.standard.set(UserString, forKey: Id)
    
    
    
    
}

func GetDataSignIn() -> String
{
    
    let Id = "IdDataUser"

    var data = ""
    if let ss = UserDefaults.standard.object(forKey: Id)
    {

        data = ss as! String

    }

    return data
}

func setDataListCustomer(listCustomer: String)//, product:String)
{
    let Id = "IdDataListCustomer"
 
    UserDefaults.standard.set(listCustomer, forKey: Id)
    
}

func GetDataListCustomer() -> String
{
    let Id = "IdDataListCustomer"
    
    var data = ""
    if let ss = UserDefaults.standard.object(forKey: Id)
    {
        
        data = ss as! String
        
        
    }

    return data
}

func setDataLogs(listCustomer: [String])//, product:String)
{
    let Id = "IdDataLogs"
    
    UserDefaults.standard.set(listCustomer, forKey: Id)
    
}

func GetDataLogsr() -> [String]
{
    let Id = "IdDataLogs"
    
    var data = [String]()
    if let ss = UserDefaults.standard.object(forKey: Id)
    {
        
        data = ss as! [String]
        
        
    }
    
    return data
}


