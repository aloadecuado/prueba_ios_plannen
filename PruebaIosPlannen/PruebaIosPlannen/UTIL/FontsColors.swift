//
//  FontsColors.swift
//  co.facilpay.facilpayiostemporal
//
//  Created by Pedro Alonso Daza B on 26/09/18.
//  Copyright © 2018 Pedro Alonso Daza B. All rights reserved.
//

import Foundation
import UIKit
import Material


let ColorPing = UIColorFromRGB(rgbValue:0xFC4262)
let ColorErrorRed = UIColorFromRGB(rgbValue:0xFC4262)

func UIColorFromRGB(rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
