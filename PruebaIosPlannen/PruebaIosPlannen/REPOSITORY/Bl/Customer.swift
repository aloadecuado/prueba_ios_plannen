//
//  Customer.swift
//  PruebaIosPlannen
//
//  Created by Propio Data on 6/5/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
import UIKit
import SwiftSpinner

var Logs = [String]()
func getListCustomer(controller:UIViewController, userId:String, Ok:@escaping (([CustomerModel]) -> Void)){
    
    
    if GetDataListCustomer() != ""{
        Ok(getListCustomer())
        return
    }
    let url = KURLListCustomer.replacingOccurrences(of: "{userId}", with: userId)
    
    SwiftSpinner.show("")
    getArray(url: url, Ok: {(array, ss) in
        SwiftSpinner.hide()
        var discCustomer = [CustomerModel]()
        
        for dic in array{
            let customer = CustomerModel(dic: dic as! NSDictionary)
            discCustomer.append(customer)
        }
        
        if discCustomer.count >= 1{
            setDataListCustomer(listCustomer: ss)
            Ok(discCustomer)
        }else{
            showAlertError(View: controller, Men: "error")
        }
    }, Error: {error in
        SwiftSpinner.hide()
        showAlertError(View: controller, Men: error)
        
    })
}

func saveData(customer:CustomerModel){
    
    var Logs = GetDataLogsr()
    let cutomers = getListCustomer()
    var cutomersSave = [CustomerModel]()
     var log = ""
    for custom in cutomers{
        if custom.customerId == customer.customerId{
            cutomersSave.append(customer)
            let name = (custom.nombre != customer.nombre) ? "nombe: \(customer.nombre)":""
            let lasName = (custom.apellido != customer.apellido) ? ", apellido: \(customer.apellido)":""
            let phone = (custom.telefono != customer.telefono) ? ", telefono: \(customer.telefono)":""
            let address = (custom.direccion != customer.direccion) ? ", direccion: \(customer.direccion)":""
            let state = (custom.estado != customer.estado) ? ", estado: \(customer.estado)":""
            
            
            
            log = "el usuario con id \(USER.userId) hizo cambios en: \(name)\(lasName)\(phone)\(address)\(state)"
            
            Logs.append(log)
        }else{
            cutomersSave.append(custom)
        }
    }
    let arrayJson = getJsonArrayString(array: cutomersSave)
    setDataListCustomer(listCustomer: arrayJson)
    setDataLogs(listCustomer: Logs)
}
func getListCustomer() -> [CustomerModel]{
    
    let ss = GetDataListCustomer()
    if ss == ""{
        return [CustomerModel]()
    }
    let array = convertToArray(text: ss)! as NSArray
    var discCustomer = [CustomerModel]()
    
    for dic in array{
        let customer = CustomerModel(dic: dic as! NSDictionary)
        discCustomer.append(customer)
    }
    
    
    return discCustomer
}
