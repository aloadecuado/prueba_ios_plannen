//
//  User.swift
//  PruebaIosPlannen
//
//  Created by Propio Data on 6/5/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
import UIKit

func signIn(controller:UIViewController, view:UIView, etEmail:IndetTextField, etPass:IndetTextField, Ok:@escaping ((UserModel) -> Void)){
    

    if(ValidationEmptyExeption(viewc: view, Ets: [IndetTextField]())){
        let url = KURLUser.replacingOccurrences(of: "{email}", with: etEmail.text!).replacingOccurrences(of: "{pass}", with: etPass.text!)
        
        getDictionaryAndString(url: url, Ok: {(dic, ss) in
            
            let user = UserModel(dic: dic)
            if user.succes{
                
                setDataSignIn(UserString: ss)
                Ok(user)
            }else{
                showAlertError(View: controller, Men: user.resultMessage)
            }
        }, Error: {error in
            showAlertError(View: controller, Men: error)
        })
    }
    
}

func getAutoSignIn() -> UserModel{
    
    let ss = GetDataSignIn()
    if ss == ""{
        return UserModel(dic: ["":""])
    }
    let dic = convertToDictionary(text: ss)! as NSDictionary
    let user = UserModel(dic: dic)
    
    return user
}

func signOutS(){
    setDataSignIn(UserString: "")
    setDataListCustomer(listCustomer: "")
}


