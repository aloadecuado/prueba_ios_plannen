//
//  GetRequest.swift
//  PruebaIosPlannen
//
//  Created by Propio Data on 6/5/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import Foundation
import Alamofire


func getDictionaryAndString(url:String, Ok:@escaping ((NSDictionary, String) -> Void), Error:@escaping ((String) -> Void))
{
    
    
    print("Url: \(url)")
    request(url, method: .get, encoding: URLEncoding.default)
        .responseData {response in
            print("error code status: \(String(describing: response.response?.statusCode))")
            if let err = response.result.error {
                print("error code: \(err.localizedDescription)")
                print("error: \(err.localizedDescription)")
                Error( "En el momento la aplicación no está disponible intente más tarde")
            }
                
            else if (response.response?.statusCode == 200 || response.response?.statusCode == 201 || response.response?.statusCode == 422)
            {
                
                if let data = response.data{
                    
                    let strs = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    var str = "";
                    if let sss = strs{
                        str = sss as String
                    }
                
                    print("Json muestra: \(str)")
                    var json:NSDictionary?// = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
                    

                    

                    
                    do {
                        json = try (JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary)
                        
                        DispatchQueue.main.async() {
                            
                            
                            if json != nil
                            {
                                Ok(json!, str)
                            }
                            else
                            {
                                Error("Error de codificacion")
                            }
                            
                            
                            
                        }
                        // use anyObj here
                    } catch {
                        Error( "\(error)")
                        print("json error: \(error)")
                        return
                    }
                    
                    
                    
                }
                
            }
            else
            {
                Error("Error de codificacion")
                Error( "Error de codificacion")
            }
    }
    
    
    
}

func getArray(url:String, Ok:@escaping ((NSArray, String) -> Void), Error:@escaping ((String) -> Void))
{
    
    
    print("Url: \(url)")
    request(url, method: .get, encoding: URLEncoding.default)
        .responseData {response in
            print("error code status: \(String(describing: response.response?.statusCode))")
            if let err = response.result.error {
                print("error code: \(err.localizedDescription)")
                print("error: \(err.localizedDescription)")
                Error( "En el momento la aplicación no está disponible intente más tarde")
            }
                
            else if (response.response?.statusCode == 200 || response.response?.statusCode == 201 || response.response?.statusCode == 422)
            {
                
                if let data = response.data{
                    
                    let strs = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    var str = "";
                    if let sss = strs{
                        str = sss as String
                    }
                    
                    print("Json muestra: \(str)")
                    var array:NSArray?// = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
                    
                    
                    
                    do {
                        array = try (JSONSerialization.jsonObject(with: data, options: []) as? NSArray)
                        
                        DispatchQueue.main.async() {
                            
                            
                            if array != nil
                            {
                                Ok(array!, str)
                            }
                            else
                            {
                                Error("Error de codificacion")
                            }
                            
                            
                            
                        }
                        // use anyObj here
                    } catch {
                        Error( "\(error)")
                        print("json error: \(error)")
                        return
                    }
                    
                    
                    
                }
                
            }
            else
            {
                Error("Error de codificacion")
                Error( "Error de codificacion")
            }
    }
    
    
    
}
